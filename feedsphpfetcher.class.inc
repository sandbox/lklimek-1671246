<?php
/**
 * @file
 *
 * This file contains implementation of Feeds PHP Fetcher. It is a plugin for
 * Feeds module.
 *
 */
/**
 * @class
 * This class represents Feeds PHP Fetcher, which allows you to enter your own
 * PHP code that will fetch data from remote source.
 *
 * After fetch, you should convert retrieved data to CSV or serialize it using serialize().
 * This data is returned and parsed by FeedsCSVParser of FeedsArrayParser respectively.
 *
 * @author Lukasz Klimek http://www.klimek.ws
 *
 */
class FeedsPhpFetcher extends FeedsFetcher {

  function configForm() {

    user_access('configure php fetcher') || drupal_access_denied();

    $form = array();

    $form['php_code'] = array(
      '#type' => 'textarea',
      '#title' => t('Fetching PHP code'),
      '#description' => t('Enter PHP code that will fetch required data. '
        . 'Ensure it\'s returning STRING containing CSV or serialized array '
        . '(or whatever your parser can parse). '),
      '#default_value' => $this -> config['php_code'],
    );

    $form['validate_button'] = array(
      '#type' => 'button',
      '#value' => t('Validate')
    );

    return $form;
  }// configForm()

  function configDefaults() {
    return array('php_code' => 'return "";', );
  }

  function configFormValidate(&$data) {
    user_access('configure php fetcher') || drupal_access_denied();

    $code = $data['php_code'];

    // check if content starts with <?php or <? . If so, alert user that
    // we don't need this.
    if (strpos(trim($code), '<?') === 0) {
      form_set_error("php_code", t("You should not include PHP starting tags " .
        "(<strong>&lt;?, &lt;?php </strong>) on begin of PHP code. It will be " .
        "added automatically."));
    }

    // We validate only if "validate" button was pressed.
    if (!isset($data['op']) || $data['op'] != t('Validate')) {
      return;
    }


    $eval = eval($code);
    if ($eval === FALSE) {
      form_set_error("php_code", t("Parse error or your code returned FALSE."));
    }
    elseif (!is_string($eval)) {
      form_set_error("php_code", t("Your code MUST return STRING"));
    }
    else {
      drupal_set_message(t("Your code looks OK. It returned:") . "<br/>" . nl2br(htmlentities($eval)),
        'status');
    }

    drupal_set_message(t('Your changes were NOT saved.'), 'warning');
  }

  public function save() {
    /** @todo I don't know why, but here feeds_importer($this -> id)->fetcher != $this if we
     * have validated and then saved our form. Without validation everything works fine.
     *
     * After hours of debugging I've done an ugly workaround.
     */

    /* This is a test that verifies if fetcher is correct.
    if (feeds_importer($this -> id) -> fetcher == $this) {
      drupal_set_message("Hooray!");
    } else drupal_set_message("Oooh... :("); */
    feeds_importer($this -> id)->fetcher->addConfig($this->getConfig());

    parent::save();
  }

  function fetch(FeedsSource $source) {
    $config = $this->getConfig();
    $script = $config['php_code'];
    $eval = eval($script);

    if ($eval === FALSE) {
      throw new Exception("Parse error. PHP Fetcher code is invalid.");
    }

    return new FeedsFetcherResult($eval);
  }

} //class FeedsPhpFetcher
